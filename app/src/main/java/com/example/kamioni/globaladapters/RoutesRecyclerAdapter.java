package com.example.kamioni.globaladapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kamioni.R;
import com.example.kamioni.databinding.SingleRouteItemBinding;
import com.example.kamioni.model.Offer;

import java.util.ArrayList;

public class RoutesRecyclerAdapter extends RecyclerView.Adapter<RoutesRecyclerAdapter.RoutesViewHolder> {

    public ArrayList<Offer> list = new ArrayList<>();
    public OnClicksListener listener;


    public void setList(ArrayList<Offer> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public RoutesRecyclerAdapter(OnClicksListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RoutesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        SingleRouteItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_route_item,parent,false);
        return new RoutesViewHolder(binding,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull RoutesViewHolder holder, int position) {
        holder.binding.setModel(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ?  0:list.size();
    }

    public class RoutesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        SingleRouteItemBinding binding;
        OnClicksListener onClicksListener;
        public RoutesViewHolder(@NonNull SingleRouteItemBinding itemView, OnClicksListener onClicksListener) {
            super(itemView.getRoot());
            binding = itemView;
            this.onClicksListener = onClicksListener;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClicksListener.onRouteClick(RoutesRecyclerAdapter.this.list.get(getAdapterPosition()).id);
        }
    }

    public interface OnClicksListener {
        void onRouteClick(int id);

    }
}
