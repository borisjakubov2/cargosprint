package com.example.kamioni.ui.options.settings;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentSettingsBinding;
import com.example.kamioni.ui.main.MainActivity;

import java.util.Locale;


public class SettingsFragment extends Fragment {

    private FragmentSettingsBinding binding;
    private SettingsViewModel viewModel;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_settings,container,false);
        viewModel = new ViewModelProvider(requireActivity()).get(SettingsViewModel.class);
        binding.setViewmodel(viewModel);

        init();
        listeners();

        return binding.getRoot();
    }

    private void init() {
        if(viewModel.getLanguage().equals("sr")){
            binding.radioSrb.setChecked(true);
            binding.radioEng.setChecked(false);
        }else{
            binding.radioEng.setChecked(true);
            binding.radioSrb.setChecked(false);
        }

       binding.notificationswitch.setChecked(viewModel.getNotificationChecked());

    }

    private void listeners(){
        binding.langGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == binding.radioSrb.getId()){
                    viewModel.setLanguage("sr");
                    setLocale(Locale.forLanguageTag("sr"));
                }else{
                    setLocale(Locale.forLanguageTag("en"));
                    viewModel.setLanguage("en");
                }
            }
        });

        binding.notificationswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.notificationswitch.setChecked(binding.notificationswitch.isChecked());
                viewModel.setNotificationChecked(binding.notificationswitch.isChecked());
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    private void setLocale(final Locale locale) {
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        Intent mIntent = getActivity().getIntent();
        getActivity().finish();
        startActivity(mIntent);
    }
}