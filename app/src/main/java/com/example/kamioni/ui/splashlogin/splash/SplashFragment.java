package com.example.kamioni.ui.splashlogin.splash;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentSplashBinding;
import com.example.kamioni.globaladapters.SingleChoiceDialogFragment;
import com.example.kamioni.ui.main.MainActivity;

import java.util.Locale;

import static com.google.gson.reflect.TypeToken.get;


public class SplashFragment extends Fragment implements SingleChoiceDialogFragment.SingleChoiceListener {

    private NavController navController;
    FragmentSplashBinding binding;

    private SplashViewModel splashViewModel;
    public SplashFragment() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_splash,container,false);

        init();
        startAnimations();
        return binding.getRoot();
    }


    private void init() {

        splashViewModel = new ViewModelProvider(requireActivity()).get(SplashViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setSplashVM(splashViewModel);



        navController = NavHostFragment.findNavController(this);


    }

    private void startAnimations() {
        binding.ivPaleta.startAnimation(getTranslateAnimation(0));
        binding.box1.startAnimation(getTranslateAnimation(100));
        binding.box2.startAnimation(getTranslateAnimation(120));
        binding.box3.startAnimation(getTranslateAnimation(140));
        binding.box4.startAnimation(getTranslateAnimation(160));
        binding.box5.startAnimation(getTranslateAnimation(180));
        binding.box6.startAnimation(getTranslateAnimation(200));
        binding.box7.startAnimation(getTranslateAnimation(220));
        binding.box8.startAnimation(getTranslateAnimation(240));
        binding.box9.startAnimation(getTranslateAnimation(260));
        binding.box10.startAnimation(getTranslateAnimation(280));
        binding.box11.startAnimation(getTranslateAnimation(300));
        binding.box12.startAnimation(getTranslateAnimation(320));
        binding.ivphone.startAnimation(getAlphaAnimation());
        binding.box12.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
               onAnimationsEnded();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void onAnimationsEnded() {
        if(splashViewModel.getLanguage().length()<1){
            SingleChoiceDialogFragment singleChoiceDialog = new SingleChoiceDialogFragment();
            singleChoiceDialog.setmListener(this::onPositiveButtonClicked);


            singleChoiceDialog.setCancelable(false);
            singleChoiceDialog.show(getChildFragmentManager(), "Single Choice Dialog");
        }else{
            if(splashViewModel.getAccessToken().length()>1){
                String locale = splashViewModel.getLanguage();
                setLocale(Locale.forLanguageTag(locale));
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
                getActivity().finish();
                //observe();
            }
            else{
                navController.navigate(R.id.action_splashFragment_to_fragmentLogin);
            }
        }

    }

    private void observe() {
        splashViewModel.getRefreshToken();
        splashViewModel.registrationResponseMutableLiveData.observe(getViewLifecycleOwner(), tokenResponse -> splashViewModel.registrationResponseMutableLiveData.observe(getActivity(), tokenResponse1 -> {
            splashViewModel.insertUser(tokenResponse1);
            Intent i = new Intent(getActivity(), MainActivity.class);
            startActivity(i);
            getActivity().finish();
        }));

        splashViewModel.messsage.observe(getViewLifecycleOwner(), s -> Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show());
    }


    private TranslateAnimation getTranslateAnimation(int offset){
        TranslateAnimation animation = new TranslateAnimation(0,0,-500f,0);
        animation.setRepeatMode(0);
        animation.setDuration(600);
        animation.setFillAfter(true);
        animation.setStartOffset(offset);
        animation.setFillAfter(true);
        return animation;
    }

    private AlphaAnimation getAlphaAnimation(){
        AlphaAnimation animation1 = new AlphaAnimation(0, 1.0f);
        animation1.setDuration(600);
        animation1.setStartOffset(300);
        animation1.setFillAfter(true);
        return animation1;
    }


    @Override
    public void onPositiveButtonClicked(String[] list, int position) {
        switch (position){
            case 0:
                navController.navigate(R.id.action_splashFragment_to_fragmentLogin);
                setLocale(Locale.forLanguageTag("sr"));
                splashViewModel.setLanguage("sr");
                break;
            case 1:
                navController.navigate(R.id.action_splashFragment_to_fragmentLogin);
                setLocale(Locale.forLanguageTag("en"));
                splashViewModel.setLanguage("en");
                break;
        }
    }

    private void setLocale(final Locale locale) {
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

}