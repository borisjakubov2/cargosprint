package com.example.kamioni.ui.main;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.LogoutResponse;
import com.example.kamioni.repository.Repository;
import com.example.kamioni.utils.ErrorHandler;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;


public class MainViewModel extends ViewModel {


    private final Repository repository;


    @ViewModelInject
    public MainViewModel(Repository repository) {
        this.repository = repository;
    }


}
