package com.example.kamioni.ui.main.customroutes;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.Offer;
import com.example.kamioni.model.RouteResponse;
import com.example.kamioni.repository.Repository;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CustomRoutesViewModel extends ViewModel {

    public MutableLiveData<ArrayList<Offer>> myRoutesList = new MutableLiveData<>();
    private final CompositeDisposable disposables = new CompositeDisposable();
    public ObservableBoolean layoutExanded = new ObservableBoolean(true);
    public MutableLiveData<Integer> weightSlider = new MutableLiveData<>(500);
    public MutableLiveData<Integer> priceSlider = new MutableLiveData<>(20000);
    public ObservableBoolean routeEmpty = new ObservableBoolean(false);
    public ObservableBoolean progressShow = new ObservableBoolean(false);
    public MutableLiveData<String> address = new MutableLiveData<>("");
    private final Repository repository;


    @ViewModelInject
    public CustomRoutesViewModel(Repository repository) {
        this.repository = repository;
    }


    public void getSearchRoutes(){
        progressShow.set(true);
        disposables.add(repository.getSearchRoutes(weightSlider.getValue(),priceSlider.getValue(),address.getValue()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RouteResponse>() {
                               @Override
                               public void accept(RouteResponse routeResponse) throws Throwable {
                                   myRoutesList.postValue(routeResponse.data);
                                   layoutExanded.set(false);
                                   progressShow.set(false);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Throwable {
                                layoutExanded.set(false);
                                progressShow.set(false);
                            }
                        }
                ));
    }

}