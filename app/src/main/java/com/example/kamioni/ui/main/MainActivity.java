package com.example.kamioni.ui.main;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.example.kamioni.R;
import com.example.kamioni.databinding.ActivityMainBinding;
import com.example.kamioni.ui.splashlogin.SplashLoginRegisterActivity;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private NavController navController;
    private AppBarConfiguration appBarConfiguration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);


        init();
        observe();
        listeners();

    }


    private void init() {

        MainViewModel mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setMainVm(mainViewModel);


        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if(navHostFragment != null) {
            navController = navHostFragment.getNavController();
        }
        setSupportActionBar(binding.toolbar);

        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.my_routes, R.id.my_location, R.id.custom_routes,R.id.current_routes)
                .build();

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);



    }

    private void observe() {

    }

    private void listeners() {
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                int id = destination.getId();
                if(id==R.id.myProfileFragment || id == R.id.settingsFragment){
                    binding.navView.setVisibility(View.GONE);
                    binding.toolbar.setNavigationIcon(R.drawable.arrow_back);
                }
                else{
                    binding.toolbar.setTitle(R.string.app_name);
                    binding.navView.setVisibility(View.VISIBLE);

                }
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            navController.navigate(R.id.action_global_settingsFragment);
            return true;
        }

        if(id == R.id.action_my_profile){
            navController.navigate(R.id.action_global_myProfileFragment);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}