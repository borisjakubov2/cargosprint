package com.example.kamioni.ui.single;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.CompanyId;
import com.example.kamioni.model.LocalOffer;
import com.example.kamioni.model.Offer;
import com.example.kamioni.model.RouteResponse;
import com.example.kamioni.model.SingleRouteResponse;
import com.example.kamioni.model.UpdateOfferResponse;
import com.example.kamioni.repository.Repository;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SingleViewModel extends ViewModel {

    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    public MutableLiveData<Offer> offerMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<CompanyId> companyIdMutableLiveData = new MutableLiveData<>();
    public ObservableBoolean progressShow = new ObservableBoolean(true);
    public LiveData<LocalOffer> currentRoute;
    public ObservableBoolean btnEnalbed = new ObservableBoolean();
    public ObservableBoolean btnsEnabled = new ObservableBoolean();

    @ViewModelInject
    public SingleViewModel(Repository repository) {
        this.repository = repository;
        currentRoute =  repository.getOffer();
    }
    public void getRoute(int routeId){
        progressShow.set(true);
        disposables.add(repository.getSingleRoute(routeId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<SingleRouteResponse>() {
                               @Override
                               public void accept(SingleRouteResponse routeResponse) throws Throwable {
                                   offerMutableLiveData.setValue(routeResponse.data);
                                   companyIdMutableLiveData.setValue(routeResponse.data.company_id);
                                   progressShow.set(false);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Throwable {

                                progressShow.set(false);
                            }
                        }
                ));
    }



    public void updateOfferStatus(LocalOffer offer,String status){
        progressShow.set(true);
        disposables.add(repository.updateOfferStatus(offer.localId,status).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<UpdateOfferResponse>() {
            @Override
            public void accept(UpdateOfferResponse updateOfferResponse) throws Throwable {
                    if(updateOfferResponse.message!=null ){
                        if(status.equals("start")){
                            repository.insertOffer(offer);
                        }else if(status.equals("finish")){
                            repository.deleteRoute();
                        }
                        progressShow.set(false);

                    }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Throwable {
                progressShow.set(false);

            }
        }));
    }
}
