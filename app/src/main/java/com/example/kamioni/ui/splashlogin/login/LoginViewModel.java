package com.example.kamioni.ui.splashlogin.login;


import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.kamioni.model.TokenResponse;
import com.example.kamioni.repository.Repository;
import com.example.kamioni.utils.ErrorHandler;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;


public class LoginViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();

    private final Repository repository;
    public ObservableBoolean progressShow = new ObservableBoolean(false);
    public MutableLiveData<String> email = new MutableLiveData<>("");
    public MutableLiveData<String> pw = new MutableLiveData<>("");
    public MutableLiveData<TokenResponse> registrationResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<String> message = new MutableLiveData<>("");


    @ViewModelInject
    public LoginViewModel(Repository repository) {
        this.repository = repository;
    }

    public MutableLiveData<String> getEmail() {
        if (email == null) {
            email = new MutableLiveData<>();
        }
        return email;
    }

    public MutableLiveData<String> getPw() {
        if (pw == null) {
            pw = new MutableLiveData<>();
        }
        return pw;
    }

    public void insertUser(TokenResponse tokenResponse) {
        repository.insertUser(tokenResponse);
    }

    public void login() {
        progressShow.set(true);
        disposables.add(repository.loginUser(email.getValue(), pw.getValue())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        result -> {
                            registrationResponseMutableLiveData.setValue(result);
                            progressShow.set(false);

                        },
                        throwable -> {
                            LoginViewModel.this.handleError(throwable);
                            progressShow.set(false);
                        }));

    }

    private void handleError(Throwable e) {
        message.setValue(ErrorHandler.getErrorMessage(e));

    }
    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.dispose();
    }
}
