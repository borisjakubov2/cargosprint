package com.example.kamioni.ui.splashlogin.register;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.RegistrationRequest;
import com.example.kamioni.model.TokenResponse;
import com.example.kamioni.repository.Repository;
import com.example.kamioni.utils.ErrorHandler;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RegisterViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();

    private final Repository repository;
    public ObservableBoolean progressShow = new ObservableBoolean(false);
    public MutableLiveData<String> email = new MutableLiveData<>("");
    public MutableLiveData<String> pw = new MutableLiveData<>("");
    public MutableLiveData<String> repeatpw = new MutableLiveData<>("");
    public MutableLiveData<String> firstname = new MutableLiveData<>("");
    public MutableLiveData<String> lastname = new MutableLiveData<>("");
    public MutableLiveData<String> phone = new MutableLiveData<>("");
    public MutableLiveData<String> job = new MutableLiveData<>("");
    public MutableLiveData<String> address = new MutableLiveData<>("");
    public MutableLiveData<Integer> radius = new MutableLiveData<>(1);
    public MutableLiveData<String> message = new MutableLiveData<>("");

    public MutableLiveData<TokenResponse> registrationResponseMutableLiveData = new MutableLiveData<>();

    @ViewModelInject
    public RegisterViewModel(Repository repository) {
        this.repository = repository;
    }

    public void Register(RegistrationRequest registrationRequest){
        progressShow.set(true);
        disposables.add(repository.register(registrationRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(tokenResponse -> {
            progressShow.set(false);
            registrationResponseMutableLiveData.setValue(tokenResponse);
        }, throwable -> {
            RegisterViewModel.this.handleError(throwable);
            progressShow.set(false);
        }
        ));

    }

    private void handleError(Throwable e) {
        message.setValue(ErrorHandler.getErrorMessage(e));

    }

    public void insertUser(TokenResponse tokenResponse){
        repository.insertUser(tokenResponse);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.dispose();
    }
}
