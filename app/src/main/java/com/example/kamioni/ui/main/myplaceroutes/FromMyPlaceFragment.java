package com.example.kamioni.ui.main.myplaceroutes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentMyPlaceBinding;
import com.example.kamioni.globaladapters.RoutesRecyclerAdapter;
import com.example.kamioni.ui.single.SingleRoute;


public class FromMyPlaceFragment extends Fragment implements RoutesRecyclerAdapter.OnClicksListener {

    private FromMyPlaceViewModel myPlaceViewModel;
    private FragmentMyPlaceBinding binding;
    private RoutesRecyclerAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        myPlaceViewModel = new ViewModelProvider(requireActivity()).get(FromMyPlaceViewModel.class);
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_my_place,container,false);
        binding.setLifecycleOwner(this);
        init();
        listeners();
        observe();
        return binding.getRoot();
    }

    private void observe() {
        myPlaceViewModel.myRoutesList.observe(getViewLifecycleOwner(), data -> {
            if(data.size()<1){
                myPlaceViewModel.routeEmpty.set(true);
            }else{
                myPlaceViewModel.routeEmpty.set(false);
                adapter.setList(data);
            }

        });
    }

    private void listeners() {
        binding.switchFromTo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            adapter.list.clear();
            if(isChecked){
                myPlaceViewModel.getMyPlaceRoutes(1);
            }else{
                myPlaceViewModel.getMyPlaceRoutes(0);
            }
        });
    }

    private void init() {
        adapter = new RoutesRecyclerAdapter(this::onRouteClick);

        adapter.setHasStableIds(true);
        binding.recyclerFirst.setAdapter(adapter);
        binding.recyclerFirst.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerFirst.setHasFixedSize(true);
        binding.recyclerFirst.setItemViewCacheSize(20);
        binding.recyclerFirst.setDrawingCacheEnabled(true);
        binding.recyclerFirst.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.setViewModel(myPlaceViewModel);
        myPlaceViewModel.routeEmpty.set(false);
        myPlaceViewModel.getMyPlaceRoutes(0);
    }

    @Override
    public void onRouteClick(int id) {
        Intent intent = new Intent(getActivity(), SingleRoute.class);
        intent.putExtra("route_id",id);
        getActivity().startActivity(intent);
    }
}