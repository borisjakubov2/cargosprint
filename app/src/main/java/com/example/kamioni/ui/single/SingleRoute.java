package com.example.kamioni.ui.single;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.kamioni.R;
import com.example.kamioni.databinding.ActivitySingleBinding;
import com.example.kamioni.model.CompanyId;
import com.example.kamioni.model.LocalOffer;
import com.example.kamioni.services.BackgroundLocationService;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import java.util.Map;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SingleRoute extends AppCompatActivity implements OnMapReadyCallback {

    private SingleViewModel singleViewModel;
    private ActivitySingleBinding binding;
    private LocationManager locationManager;
    private Intent locationIntent;
    public BackgroundLocationService gpsService;
    private int clicked_id;
    private LocalOffer localOffer;
    private boolean from;
    private GoogleMap mMap;

    ActivityResultLauncher<String[]> locationPermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            Boolean fineLocationPermission = result.get(LOCATION_PERMISSIONS()[1]);
            Boolean coarseLocationPermission = result.get(LOCATION_PERMISSIONS()[0]);
            if (!fineLocationPermission && !coarseLocationPermission) {
                Toast.makeText(getApplicationContext(),"MORATE DOZVOLITI UPOTREBU LOKACIJE",Toast.LENGTH_LONG).show();
            } else if (isLocationEnabled()) {
                promptSettings();
            } else {
                gpsService.startTracking();
            }
        }
    });

    ActivityResultLauncher<IntentSenderRequest> locationSettingsLauncher = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), new ActivityResultCallback<ActivityResult>() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onActivityResult(ActivityResult result) {
            switch (result.getResultCode()) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    startService();
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    Toast.makeText(getApplicationContext(),"MORATE UKLJUCITI LOKACIJU",Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_single);

        init();
        observe();
        listeners();
    }

    private void init(){
        setSupportActionBar(binding.toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.toolbar.setNavigationIcon(R.drawable.arrow_back);


        clicked_id = getIntent().getIntExtra("route_id",0);
        from = getIntent().getBooleanExtra("fromMy",false);

        singleViewModel = new ViewModelProvider(this).get(SingleViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setViewModel(singleViewModel);
        singleViewModel.getRoute(clicked_id);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationIntent = new Intent(this, BackgroundLocationService.class);
        locationIntent.putExtra("route_id",clicked_id);
    }

    private void observe(){
        singleViewModel.offerMutableLiveData.observe(this, offer -> {
            binding.setModel(offer);
            localOffer = new LocalOffer();
            localOffer.localId = offer.id;
            if(!from){
                binding.btmlayout.setVisibility(View.GONE);
                singleViewModel.btnsEnabled.set(false);
            }else{
                binding.btmlayout.setVisibility(View.VISIBLE);
            }
        });

        singleViewModel.currentRoute.observe(this, offer -> {
            if(offer!=null){
                singleViewModel.btnsEnabled.set(clicked_id != offer.localId);
                singleViewModel.btnEnalbed.set(true);
            }else{
                singleViewModel.btnEnalbed.set(false);
            }
        });

        singleViewModel.companyIdMutableLiveData.observe(this, new Observer<CompanyId>() {
            @Override
            public void onChanged(CompanyId companyId) {
                binding.setCompany(companyId);
            }
        });
    }

    private void listeners(){
        binding.startRouteBtn.setOnClickListener(v->{
            startTracking();
        });

        binding.stopRouteBtn.setOnClickListener(v->{

            stopService();
        });

        binding.phone.setOnClickListener(v->{
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(singleViewModel.companyIdMutableLiveData.getValue().phone));
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void startTracking(){
        if (!checkPermission()) {
            locationPermissions.launch(LOCATION_PERMISSIONS());
        }
        //prompt location settings if not enabled
        else if (!isLocationEnabled()) {
            promptSettings();
        } else {

            startService();

        }
    }

    private void startService() {
        this.bindService(locationIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        this.startService(locationIntent);
        singleViewModel.updateOfferStatus(localOffer,"start");


    }

    private void stopService(){
        this.stopService(locationIntent);
        singleViewModel.updateOfferStatus(localOffer,"finish");
        super.onBackPressed();
    }


    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return locationPermissionsEnabled(this);
        } else {
            return true;
        }
    }

    public static String[] LOCATION_PERMISSIONS() {
        return new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

    }

    public static boolean locationPermissionsEnabled(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void promptSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY));
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                if(task.isSuccessful()){
                    startService();
                }
                // All location settings are satisfied. The client can initialize location
                // requests here.

            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            IntentSenderRequest i = new IntentSenderRequest.Builder(resolvable.getResolution().getIntentSender()).build();
                            locationSettingsLauncher.launch(i);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }

        });
    }


    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            String name = className.getClassName();
            if (name.endsWith("BackgroundLocationService")) {
                gpsService = ((BackgroundLocationService.LocationServiceBinder) service).getService();
                gpsService.startTracking();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (className.getClassName().equals("BackgroundLocationService")) {
                gpsService = null;
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(44.8149028, 20.1424149);

        mMap.addMarker(new MarkerOptions()
                .position(sydney));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,11.0f));
    }
}

