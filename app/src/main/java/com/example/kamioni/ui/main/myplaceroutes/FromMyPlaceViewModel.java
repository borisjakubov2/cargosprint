package com.example.kamioni.ui.main.myplaceroutes;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.Offer;
import com.example.kamioni.model.RouteResponse;
import com.example.kamioni.repository.Repository;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class FromMyPlaceViewModel extends ViewModel {

    public MutableLiveData<ArrayList<Offer>> myRoutesList = new MutableLiveData<>();
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Repository repository;
    public ObservableBoolean progressShow = new ObservableBoolean(true);
    public ObservableBoolean routeEmpty = new ObservableBoolean(false);

    @ViewModelInject
    public FromMyPlaceViewModel(Repository repository) {
        this.repository = repository;
    }



    public void getMyPlaceRoutes(int isFinish){
        progressShow.set(true);
        disposables.add(repository.getMyPlaceRoutes(isFinish).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RouteResponse>() {
                    @Override
                    public void accept(RouteResponse routeResponse) throws Throwable {
                        myRoutesList.postValue(routeResponse.data);
                        progressShow.set(false);
                        }
                    },
                new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Throwable {

                        progressShow.set(false);
                    }
                }
        ));
    }



}