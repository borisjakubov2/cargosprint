package com.example.kamioni.ui.options.myprofile;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentMyProfileBinding;
import com.example.kamioni.model.ProfileResponse;
import com.example.kamioni.ui.splashlogin.SplashLoginRegisterActivity;
import com.google.android.material.slider.Slider;


public class MyProfileFragment extends Fragment {

    private MyProfileViewModel profileViewModel;
    private FragmentMyProfileBinding binding;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_my_profile,container,false);
        binding.setLifecycleOwner(this);
        profileViewModel = new ViewModelProvider(requireActivity()).get(MyProfileViewModel.class);
        binding.setViewModel(profileViewModel);
        init();
        observe();
        listeners();
        return binding.getRoot();
    }

    private void init() {
        profileViewModel.getMyProfile();
    }


    private void observe() {
        profileViewModel.profileResponseMutableLiveData.observe(getViewLifecycleOwner(), new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {
                binding.setProfile(profileResponse);
                binding.radiusSlider.setValue(profileResponse.radius);
                profileViewModel.radius.setValue(profileResponse.radius);

            }
        });

        profileViewModel.logOutSuccess.observe(getViewLifecycleOwner(), aBoolean -> {
            if(aBoolean){
                Intent i = new Intent(getActivity(), SplashLoginRegisterActivity.class);
                i.putExtra("fromLogOut",true);
                startActivity(i);
                getActivity().finish();

            }
        });

        profileViewModel.message.observe(getViewLifecycleOwner(), s -> {
            if(s.length()>1){
                Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listeners() {
        binding.radiusSlider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {
            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                profileViewModel.radius.setValue((int) slider.getValue());
                profileViewModel.btnShow.set(true);
            }
        });

        binding.updateBtn.setOnClickListener(v->{
            profileViewModel.updateUser();
        });

        binding.logoutBtn.setOnClickListener(v->{
            profileViewModel.doLogout();
        });
    }


    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }


    @Override
    public void onDetach() {
        profileViewModel.btnShow.set(false);
        super.onDetach();
    }
}