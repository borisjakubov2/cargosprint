package com.example.kamioni.ui.main.customroutes;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentCustomRoutesBinding;
import com.example.kamioni.globaladapters.RoutesRecyclerAdapter;
import com.example.kamioni.model.Offer;
import com.example.kamioni.ui.single.SingleRoute;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AddressComponent;
import com.google.android.libraries.places.api.model.AddressComponents;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.slider.Slider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class CustomRoutesFragment extends Fragment implements  RoutesRecyclerAdapter.OnClicksListener {

    private CustomRoutesViewModel customRoutesViewModel;
    private FragmentCustomRoutesBinding binding;
    private RoutesRecyclerAdapter adapter;


    ActivityResultLauncher<Intent> autocompleteLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(result.getData());
                        AddressComponents addressComponents = place.getAddressComponents();
                        String address = null,number = null,city=null,state=null;

                        StringBuilder s2 = new StringBuilder();
                        for(AddressComponent a : addressComponents.asList()){
                            for(String s : a.getTypes()){
                                switch (s) {
                                    case "route":
                                        address = a.getName();
                                        break;
                                    case "street_number":
                                        number = a.getName();
                                        break;
                                    case "locality":
                                        city = a.getName();
                                        break;
                                    case "country":
                                        state = a.getName();
                                        break;
                                }
                            }

                        }
                        if(address!=null && !address.isEmpty()){
                            if(number!=null &&!number.isEmpty()){
                                s2.append(address).append(" ").append(number).append(", ");
                            }else{
                                s2.append(address).append(", ");
                            }

                        }

                        if(city!=null &&!city.isEmpty()){
                            s2.append(city).append(", ");

                        }
                        if(state!=null &&!state.isEmpty()){
                            s2.append(state);
                        }
                        customRoutesViewModel.address.setValue(s2.toString());

                    }
                }
            });

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_custom_routes,container,false);
        binding.setLifecycleOwner(this);
        customRoutesViewModel = new ViewModelProvider(requireActivity()).get(CustomRoutesViewModel.class);
        init();
        listeners();
        observe();

        return binding.getRoot();
    }

    private void init() {
        adapter = new RoutesRecyclerAdapter(this);

        adapter.setHasStableIds(true);
        binding.recyclerFirst.setAdapter(adapter);
        binding.recyclerFirst.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerFirst.setHasFixedSize(true);
        binding.recyclerFirst.setItemViewCacheSize(20);
        binding.recyclerFirst.setDrawingCacheEnabled(true);
        binding.recyclerFirst.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.etAddress.setFocusable(false);
        setupPlaces();
        binding.setViewModel(customRoutesViewModel);
    }

    private void setupPlaces() {
        String apiKey = getString(R.string.api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }

        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(getContext());

        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                // Call either setLocationBias() OR setLocationRestriction().
                .setTypeFilter(TypeFilter.ADDRESS)
                .setTypeFilter(TypeFilter.CITIES)
                .build();

        placesClient.findAutocompletePredictions(request);
    }

    private void listeners() {
        binding.arrow.setOnClickListener(v->{
            customRoutesViewModel.layoutExanded.set(!customRoutesViewModel.layoutExanded.get());

        });

        binding.weightSlider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {

            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                customRoutesViewModel.weightSlider.setValue((int) slider.getValue());
            }
        });

        binding.priceslider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {

            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                customRoutesViewModel.priceSlider.setValue((int) slider.getValue());
            }
        });

        binding.btnsearch.setOnClickListener(v->{
            customRoutesViewModel.getSearchRoutes();
        });

        binding.etAddress.setOnClickListener(v -> {
            List<Place.Field> fields = Collections.singletonList(Place.Field.ADDRESS_COMPONENTS);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(getContext());
            autocompleteLauncher.launch(intent);
        });
    }


    private void observe() {
        customRoutesViewModel.myRoutesList.observe(getViewLifecycleOwner(), new Observer<ArrayList<Offer>>() {
            @Override
            public void onChanged(ArrayList<Offer> offers) {
                if(offers.size()<1){
                    customRoutesViewModel.routeEmpty.set(true);
                }
                else{
                    customRoutesViewModel.routeEmpty.set(false);
                    adapter.setList(offers);
                }
            }
        });
    }

    @Override
    public void onRouteClick(int id) {
        Intent intent = new Intent(getActivity(), SingleRoute.class);
        intent.putExtra("route_id",id);
        getActivity().startActivity(intent);
    }
}