package com.example.kamioni.ui.options.myprofile;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.ProfileResponse;
import com.example.kamioni.model.UpdateUserResponse;
import com.example.kamioni.repository.Repository;
import com.example.kamioni.utils.ErrorHandler;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MyProfileViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();
    public MutableLiveData<ProfileResponse> profileResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Integer> radius = new MutableLiveData<>(1);
    public ObservableBoolean progressShow = new ObservableBoolean(false);
    public ObservableBoolean btnShow = new ObservableBoolean(false);
    public MutableLiveData<Boolean> logOutSuccess = new MutableLiveData<>(false);
    public MutableLiveData<String> message = new MutableLiveData<>("");

    private final Repository repository;

    @ViewModelInject
    public MyProfileViewModel(Repository repository) {
        this.repository = repository;
    }


    public void getMyProfile(){
        progressShow.set(true);
        disposables.add(repository.getMyProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ProfileResponse>() {
                    @Override
                    public void accept(ProfileResponse profileResponse) throws Throwable {
                        profileResponseMutableLiveData.postValue(profileResponse);
                        progressShow.set(false);
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Throwable {
                        progressShow.set(false);
                    }
                }));
    }

    public void updateUser(){
        progressShow.set(true);
        disposables.add(repository.updateUser(radius.getValue())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<UpdateUserResponse>() {
                    @Override
                    public void accept(UpdateUserResponse profileResponse) throws Throwable {
                        progressShow.set(false);
                        btnShow.set(false);
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Throwable {
                        progressShow.set(false);
                        btnShow.set(false);
                    }
                }));
    }

    public void doLogout(){
        progressShow.set(true);
        disposables.add(repository.logoutUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(logoutResponse -> {
                            progressShow.set(false);
                            repository.deleteUser();
                            message.setValue(logoutResponse.message);
                            logOutSuccess.setValue(true);
                        }
                        , throwable -> {
                            handleError(throwable);
                            progressShow.set(false);
                        }
                ));
    }
    private void handleError(Throwable e) {
        message.setValue(ErrorHandler.getErrorMessage(e));

    }

}
