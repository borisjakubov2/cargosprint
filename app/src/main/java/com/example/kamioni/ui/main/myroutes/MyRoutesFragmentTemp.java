package com.example.kamioni.ui.main.myroutes;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentMyRoutesTempBinding;
import com.example.kamioni.services.BackgroundLocationService;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

import java.util.Map;

public class MyRoutesFragmentTemp extends Fragment {



    ActivityResultLauncher<String[]> locationPermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            Boolean fineLocationPermission = result.get(LOCATION_PERMISSIONS()[1]);
            Boolean coarseLocationPermission = result.get(LOCATION_PERMISSIONS()[0]);
            if (!fineLocationPermission && !coarseLocationPermission) {
                Toast.makeText(getContext(),"MORATE DOZVOLITI UPOTREBU LOKACIJE",Toast.LENGTH_LONG).show();
            } else if (isLocationEnabled()) {
                promptSettings();
            } else {
                gpsService.startTracking();
            }
        }
    });

    ActivityResultLauncher<IntentSenderRequest> locationSettingsLauncher = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), new ActivityResultCallback<ActivityResult>() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onActivityResult(ActivityResult result) {
            switch (result.getResultCode()) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    gpsService.startTracking();
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    Toast.makeText(getContext(),"MORATE UKLJUCITI LOKACIJU",Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    });

    private FragmentMyRoutesTempBinding fragmentMyRoutesTempBinding;
    public BackgroundLocationService gpsService;
    private LocationManager locationManager;
    public boolean mTracking = false;
    private Intent locationIntent;
    private MyRoutesViewModel myRoutesViewModel;

    public MyRoutesFragmentTemp() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentMyRoutesTempBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_routes_temp,container,false);
        return  fragmentMyRoutesTempBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view,@Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        //myRoutesViewModel = new ViewModelProvider(requireActivity()).get(MyRoutesViewModel.class);

        // TODO: Use the ViewModel
        init();
        listeners();
    }

    private void init() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationIntent = new Intent(getContext(), BackgroundLocationService.class);


    }

    private void listeners() {
        fragmentMyRoutesTempBinding.startRouteBtn.setOnClickListener(v->{
            startTracking();
        });

        fragmentMyRoutesTempBinding.stopRouteBtn.setOnClickListener(v->{

            mTracking = false;
            toggleButtons(mTracking);
            stopService();


        });
    }

    private void startTracking(){
        if (!checkPermission()) {
            locationPermissions.launch(LOCATION_PERMISSIONS());
        }
        //prompt location settings if not enabled
        else if (!isLocationEnabled()) {
            promptSettings();
        } else {
            mTracking = true;
            toggleButtons(mTracking);
            startService();

        }
    }

    private void startService() {
        getActivity().bindService(locationIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        getActivity().startService(locationIntent);
    }

    private void stopService(){
        getActivity().stopService(locationIntent);
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return locationPermissionsEnabled(getActivity());
        } else {
            return true;
        }
    }

    private void toggleButtons(boolean mTracking) {
        fragmentMyRoutesTempBinding.startRouteBtn.setEnabled(!mTracking);
        fragmentMyRoutesTempBinding.stopRouteBtn.setEnabled(mTracking);

    }

    public static boolean locationPermissionsEnabled(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static String[] LOCATION_PERMISSIONS() {
        return new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void promptSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY));
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                if(task.isSuccessful()){
                    startService();
                }
                // All location settings are satisfied. The client can initialize location
                // requests here.

            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            IntentSenderRequest i = new IntentSenderRequest.Builder(resolvable.getResolution().getIntentSender()).build();
                            locationSettingsLauncher.launch(i);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }

        });
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            String name = className.getClassName();
            if (name.endsWith("BackgroundLocationService")) {
                gpsService = ((BackgroundLocationService.LocationServiceBinder) service).getService();
                gpsService.startTracking();
                toggleButtons(true);
                Log.d("testtest", "onServiceConnected: ");

            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (className.getClassName().equals("BackgroundLocationService")) {
                gpsService = null;
                Log.d("testtest", "onServiceDisconnected: ");
            }
        }
    };

}