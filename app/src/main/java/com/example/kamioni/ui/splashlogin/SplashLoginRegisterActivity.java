package com.example.kamioni.ui.splashlogin;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.kamioni.R;
import com.example.kamioni.databinding.ActivitySplashLoginRegisterBinding;

import java.util.Objects;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SplashLoginRegisterActivity extends AppCompatActivity {
    private NavController navController;
    private ActivitySplashLoginRegisterBinding binding;
    private AppBarConfiguration appBarConfiguration;
    private boolean fromLogout = false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_splash_login_register);

        setSupportActionBar(binding.toolbarSplash);
        fromLogout = getIntent().getBooleanExtra("fromLogOut",false);
        init();
        listeners();







    }

    private void listeners() {
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if(destination.getId()==R.id.splashFragment){
                    binding.toolbarSplash.setVisibility(View.GONE);
                }
                else{
                    getSupportActionBar().setTitle("");
                    binding.toolbarSplash.setVisibility(View.VISIBLE);
                    binding.toolbarTitle.setText(destination.getLabel());
                }

            }
        });

    }

    private void init() {
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if (navHostFragment != null) {
            navController = navHostFragment.getNavController();
        }


        appBarConfiguration = new AppBarConfiguration.Builder(R.id.fragmentLogin,R.id.splashFragment).build();

        NavigationUI.setupWithNavController(
                binding.toolbarSplash, navController, appBarConfiguration);
        if(fromLogout){
            navController.navigate(R.id.fragmentLogin);
        }
    }

    @Override
    public void onBackPressed() {
      if(Objects.requireNonNull(navController.getCurrentDestination()).getId() == R.id.fragmentRegister){
            navController.popBackStack();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

}
