package com.example.kamioni.ui.splashlogin.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentLoginBinding;
import com.example.kamioni.ui.main.MainActivity;
import com.google.android.material.textfield.TextInputLayout;


public class FragmentLogin extends Fragment {

    FragmentLoginBinding binding;
    private LoginViewModel loginViewModel;

    public FragmentLogin() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_login,container,false);
        binding.setLifecycleOwner(this);
        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        binding.setLoginvm(loginViewModel);
        return binding.getRoot();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
        observe();
        listeners();

        super.onViewCreated(view, savedInstanceState);
    }

    private void init() {
        binding.setLoginvm(loginViewModel);
    }

    private void observe() {
        loginViewModel.registrationResponseMutableLiveData.observe(getViewLifecycleOwner(), tokenResponse -> {
            loginViewModel.insertUser(tokenResponse);
            Intent i = new Intent(getActivity(), MainActivity.class);
            startActivity(i);
            getActivity().finish();
        });

        loginViewModel.getPw().observe(getViewLifecycleOwner(), s -> binding.pwEt.setError(null));

        loginViewModel.message.observe(getViewLifecycleOwner(), s -> {
            if(s.length()>1){
                Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void listeners() {
        binding.loginBtn.setOnClickListener(v->{
            if(checkEmail(binding.emailEt,loginViewModel.getEmail().getValue()) && checkPassword(binding.pwEt,loginViewModel.getPw().getValue()))
                loginViewModel.login();

        });
        binding.registerBtn.setOnClickListener(v->{
            NavHostFragment.findNavController(FragmentLogin.this)
                    .navigate(R.id.action_fragmentLogin_to_fragmentRegister);
        });
    }

    public  boolean checkEmail( TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if (charsequence.length()<1) {
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_email));
            checkPass = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(charsequence).matches()) {
            textInputLayout.setError(getContext().getResources().getString(R.string.valid_email));
            checkPass = false;
        } else {
            textInputLayout.setError(null);
            checkPass = true;

        }
        return checkPass;
    }

    public  boolean checkPassword( TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if(charsequence.length()<1){
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_pw));
            checkPass =  false;
        }
        else{
            textInputLayout.setError(null);
            checkPass = true;
        }
        return checkPass;
    }

}