package com.example.kamioni.ui.splashlogin.splash;

import androidx.databinding.ObservableBoolean;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.model.TokenResponse;
import com.example.kamioni.repository.Repository;
import com.example.kamioni.utils.ErrorHandler;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SplashViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Repository repository;
    public ObservableBoolean progressShow = new ObservableBoolean(false);
    public MutableLiveData<String> messsage = new MutableLiveData<>();

    public MutableLiveData<TokenResponse> registrationResponseMutableLiveData = new MutableLiveData<>();

    @ViewModelInject
    public SplashViewModel(Repository repository) {
        this.repository = repository;
    }


    public void insertUser(TokenResponse tokenResponse){
        repository.insertUser(tokenResponse);
    }

    public void getRefreshToken(){
        progressShow.set(true);
        disposables.add(repository.getRefreshToken(getRefreshTokenF())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<TokenResponse>() {
                            @Override
                            public void accept(TokenResponse result) throws Throwable {
                                progressShow.set(false);
                                registrationResponseMutableLiveData.setValue(result);

                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Throwable {
                                SplashViewModel.this.handleError(throwable);
                                progressShow.set(false);
                            }
                        }));
    }

    private void handleError(Throwable throwable) {
       messsage.setValue( ErrorHandler.getErrorMessage(throwable));

    }

    public String getAccessToken(){
       return repository.getAccessToken();
    }

    public String getRefreshTokenF(){
        return repository.getRefreshToken();
    }

    public String getLanguage(){
        return repository.getLanguage();
    }

    public void setLanguage(String language){
        repository.setLanguage(language);
    }

}
