package com.example.kamioni.ui.main.nearmeroutes;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentNearMeBinding;
import com.example.kamioni.globaladapters.RoutesRecyclerAdapter;
import com.example.kamioni.model.Offer;
import com.example.kamioni.ui.single.SingleRoute;
import com.example.kamioni.utils.CurrentLocationHelper;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import java.util.ArrayList;
import java.util.Map;


public class NearMeFragment extends Fragment implements CurrentLocationHelper.LocationSetListener, RoutesRecyclerAdapter.OnClicksListener {

    private NearMeViewModel nearMeViewModel;
    private FragmentNearMeBinding binding;
    private CurrentLocationHelper currentLocationHelper;
    private LocationManager locationManager;
    private RoutesRecyclerAdapter adapter;
    private double lat;
    private double longitude;

    ActivityResultLauncher<String[]> locationPermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            Boolean fineLocationPermission = result.get(LOCATION_PERMISSIONS()[1]);
            Boolean coarseLocationPermission = result.get(LOCATION_PERMISSIONS()[0]);
            if (!fineLocationPermission && !coarseLocationPermission) {
                Toast.makeText(getContext(),"MORATE DOZVOLITI UPOTREBU LOKACIJE",Toast.LENGTH_LONG).show();
            } else if (isLocationEnabled()) {
                promptSettings();
            } else {
                getLocation();
            }
        }
    });

    ActivityResultLauncher<IntentSenderRequest> locationSettingsLauncher = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            switch (result.getResultCode()) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    currentLocationHelper.requestNewLocationData();
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    Toast.makeText(getContext(),"MORATE UKLJUCITI LOKACIJU",Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    });

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        nearMeViewModel = new ViewModelProvider(requireActivity()).get(NearMeViewModel.class);

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_near_me,container,false);
        init();
        observe();
        listeners();

        return binding.getRoot();
    }

    private void listeners() {
        binding.switchFromTo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.list.clear();
                if(isChecked){
                    nearMeViewModel.getMyLocationRoutes(NearMeFragment.this.lat,NearMeFragment.this.longitude,1);
                }else{
                    nearMeViewModel.getMyLocationRoutes(NearMeFragment.this.lat,NearMeFragment.this.longitude,0);
                }
            }
        });
    }

    private void init() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        currentLocationHelper = new CurrentLocationHelper(getActivity());
        currentLocationHelper.setLocationSetListener(this);

        adapter = new RoutesRecyclerAdapter(this);

        adapter.setHasStableIds(true);
        binding.recyclerFirst.setAdapter(adapter);
        binding.recyclerFirst.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerFirst.setHasFixedSize(true);
        binding.recyclerFirst.setItemViewCacheSize(20);
        binding.recyclerFirst.setDrawingCacheEnabled(true);
        binding.recyclerFirst.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.setViewModel(nearMeViewModel);
        nearMeViewModel.progressShow.set(true);
        nearMeViewModel.routeEmpty.set(false);
        getLocation();

    }

    private void observe() {
        nearMeViewModel.myRoutesList.observe(getViewLifecycleOwner(), new Observer<ArrayList<Offer>>() {
            @Override
            public void onChanged(ArrayList<Offer> data) {
                if(data.size()<1){
                    nearMeViewModel.routeEmpty.set(true);
                }
                else{
                    nearMeViewModel.routeEmpty.set(false);
                    adapter.setList(data);
                }

            }
        });
    }


    private void getLocation(){
        if (!checkPermission()) {
            locationPermissions.launch(LOCATION_PERMISSIONS());
        }
        //prompt location settings if not enabled
        else if (!isLocationEnabled()) {
            promptSettings();
        } else {
            currentLocationHelper.requestNewLocationData();
        }
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public static String[] LOCATION_PERMISSIONS() {
        return new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return locationPermissionsEnabled(getActivity());
        } else {
            return true;
        }
    }

    public static boolean locationPermissionsEnabled(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void promptSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY));
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    currentLocationHelper.requestNewLocationData();

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                IntentSenderRequest i = new IntentSenderRequest.Builder(resolvable.getResolution().getIntentSender()).build();
                                locationSettingsLauncher.launch(i);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }

            }
        });
    }

    @Override
    public void onLocationUpdate(Location location) {
        this.lat = location.getLatitude();
        this.longitude = location.getLongitude();
        int isFinish;
        if(binding.switchFromTo.isChecked()){
            isFinish = 1;
        }else{
            isFinish = 0;
        }
        nearMeViewModel.getMyLocationRoutes(location.getLatitude(),location.getLongitude(),isFinish);
    }

    @Override
    public void onRouteClick(int id) {
        Intent intent = new Intent(getActivity(), SingleRoute.class);
        intent.putExtra("route_id",id);
        getActivity().startActivity(intent);
    }
}