package com.example.kamioni.ui.splashlogin.register;


import android.content.Intent;
import android.os.Bundle;
import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentRegisterBinding;
import com.example.kamioni.model.RegistrationRequest;
import com.example.kamioni.ui.main.MainActivity;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AddressComponent;
import com.google.android.libraries.places.api.model.AddressComponents;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.slider.Slider;
import com.google.android.material.textfield.TextInputLayout;
import java.util.Collections;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class FragmentRegister extends Fragment {

    FragmentRegisterBinding binding;
    private RegisterViewModel registerViewModel;

    public FragmentRegister() {
        // Required empty public constructor
    }

    ActivityResultLauncher<Intent> autocompleteLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(result.getData());
                        AddressComponents addressComponents = place.getAddressComponents();
                        String address = null,number = null,city=null,state=null;

                        StringBuilder s2 = new StringBuilder();
                       for(AddressComponent a : addressComponents.asList()){
                           for(String s : a.getTypes()){
                               switch (s) {
                                   case "route":
                                       address = a.getName();
                                       break;
                                   case "street_number":
                                       number = a.getName();
                                       break;
                                   case "locality":
                                       city = a.getName();
                                       break;
                                   case "country":
                                       state = a.getName();
                                       break;
                               }
                           }

                       }
                       if(address!=null && !address.isEmpty()){
                           if(number!=null &&!number.isEmpty()){
                               s2.append(address).append(" ").append(number).append(", ");
                           }else{
                               s2.append(address).append(", ");
                           }

                       }

                       if(city!=null &&!city.isEmpty()){
                           s2.append(city).append(", ");

                       }
                       if(state!=null &&!state.isEmpty()){
                           s2.append(state);
                       }
                       registerViewModel.address.setValue(s2.toString());

                    }
                }
            });


    private void init() {

        String apiKey = getString(R.string.api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }

        // Create a new Places client instance.
       PlacesClient placesClient = Places.createClient(getContext());

        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                // Call either setLocationBias() OR setLocationRestriction().
                .setTypeFilter(TypeFilter.ADDRESS)
                .setTypeFilter(TypeFilter.CITIES)
                .build();

        placesClient.findAutocompletePredictions(request);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_register,container,false);
        registerViewModel = new ViewModelProvider(requireActivity()).get(RegisterViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setRegistervm(registerViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
        observe();
        listeners();
        super.onViewCreated(view, savedInstanceState);
    }

    private void observe() {
        binding.radiusSlider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {
            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                registerViewModel.radius.setValue((int) slider.getValue());
            }
        });

        registerViewModel.registrationResponseMutableLiveData.observe(getViewLifecycleOwner(), tokenResponse -> {
            registerViewModel.insertUser(tokenResponse);
            Intent i = new Intent(getActivity(), MainActivity.class);
            startActivity(i);
            getActivity().finish();
        });

        registerViewModel.message.observe(getViewLifecycleOwner(), s -> {
            if(s.length()>1){
                Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
            }
        });

        registerViewModel.pw.observe(getViewLifecycleOwner(), s -> binding.pwEt.setError(null));
        registerViewModel.repeatpw.observe(getViewLifecycleOwner(), s -> binding.pwEtrepeat.setError(null));

    }

    private void listeners() {
        binding.registerBtn.setOnClickListener(v->{
           if(checkName(binding.nameLayout,registerViewModel.firstname.getValue())
                   && checkName(binding.lastnameLayout,registerViewModel.lastname.getValue())
                   && checkName(binding.phoneLayout,registerViewModel.phone.getValue())
                   && checkEmail(binding.emLayout,registerViewModel.email.getValue())
                   && checkPassword(binding.pwEt,registerViewModel.pw.getValue())
                   && checkRepeatPassword(binding.pwEtrepeat,registerViewModel.repeatpw.getValue())
                   && checkName(binding.jobLayout,registerViewModel.job.getValue())
                   && checkName(binding.adressLayout,registerViewModel.address.getValue()))
           {

               RegistrationRequest registrationRequest = new RegistrationRequest();
               registrationRequest.email = registerViewModel.email.getValue();
               registrationRequest.address = registerViewModel.address.getValue();
               registrationRequest.first_name = registerViewModel.firstname.getValue();
               registrationRequest.last_name = registerViewModel.lastname.getValue();
               registrationRequest.job = registerViewModel.job.getValue();
               registrationRequest.password = registerViewModel.pw.getValue();
               registrationRequest.password_confirmation = registerViewModel.repeatpw.getValue();
               registrationRequest.phone = registerViewModel.phone.getValue();
               registrationRequest.radius = registerViewModel.radius.getValue();
               registerViewModel.Register(registrationRequest);
            }

        });

        binding.etAddress.setFocusable(false);

        binding.etAddress.setOnClickListener(v -> {
            List<Place.Field> fields = Collections.singletonList(Place.Field.ADDRESS_COMPONENTS);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(getContext());
            autocompleteLauncher.launch(intent);

        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                NavHostFragment.findNavController(FragmentRegister.this)
                        .navigate(R.id.fragmentLogin);        }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

    }

    public  boolean checkEmail(TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if (charsequence.length()<1) {
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_email));
            checkPass = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(charsequence).matches()) {
            textInputLayout.setError(getContext().getResources().getString(R.string.valid_email));
            checkPass = false;
        } else {
            textInputLayout.setError(null);
            checkPass = true;

        }
        return checkPass;
    }

    public  boolean checkPassword( TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if(charsequence.length()<1){
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_field));
            checkPass =  false;
        }
        else{
            textInputLayout.setError(null);
            checkPass = true;
        }
        return checkPass;
    }

    public  boolean checkRepeatPassword( TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if(charsequence.length()<1){
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_field));
            checkPass =  false;
        }
        else if(!charsequence.equals(registerViewModel.repeatpw.getValue())){
            textInputLayout.setError(getContext().getResources().getString(R.string.repeat_pw));
            checkPass = false;

        }
        else{
            textInputLayout.setError(null);
            checkPass = true;
        }
        return checkPass;
    }

    public  boolean checkName( TextInputLayout textInputLayout, String charsequence ) {
        boolean checkPass;
        if(charsequence.length()<1){
            textInputLayout.setError(getContext().getResources().getString(R.string.enter_field));
            checkPass =  false;
        }
        else{
            textInputLayout.setError(null);
            checkPass = true;
        }
        return checkPass;
    }

}