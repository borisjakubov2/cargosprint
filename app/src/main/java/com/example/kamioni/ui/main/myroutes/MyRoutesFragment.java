package com.example.kamioni.ui.main.myroutes;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.kamioni.R;
import com.example.kamioni.databinding.FragmentMyRoutesBinding;
import com.example.kamioni.globaladapters.RoutesRecyclerAdapter;
import com.example.kamioni.ui.single.SingleRoute;

public class MyRoutesFragment extends Fragment implements RoutesRecyclerAdapter.OnClicksListener {
    private FragmentMyRoutesBinding myRoutesBinding;
    private RoutesRecyclerAdapter adapter;
    private MyRoutesViewModel myRoutesViewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myRoutesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_routes,container,false);
        myRoutesViewModel = new ViewModelProvider(requireActivity()).get(MyRoutesViewModel.class);
        myRoutesBinding.setViewModel(myRoutesViewModel);
        myRoutesBinding.setLifecycleOwner(this);
        init();
        listeners();
        observe();
        return  myRoutesBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void observe() {
        myRoutesViewModel.myRoutesList.observe(getViewLifecycleOwner(), data -> {
            if(data.size()<1){
                myRoutesViewModel.routeEmpty.set(true);
            }else{
                adapter.setList(data);
            }

        });
    }

    private void listeners() {
    }

    private void init() {
        adapter = new RoutesRecyclerAdapter(this::onRouteClick);

        adapter.setHasStableIds(true);
        myRoutesBinding.recyclerFirst.setAdapter(adapter);
        myRoutesBinding.recyclerFirst.setLayoutManager(new LinearLayoutManager(getContext()));
        myRoutesBinding.recyclerFirst.setHasFixedSize(true);
        myRoutesBinding.recyclerFirst.setItemViewCacheSize(20);
        myRoutesBinding.recyclerFirst.setDrawingCacheEnabled(true);
        myRoutesBinding.recyclerFirst.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        myRoutesViewModel.routeEmpty.set(false);
        myRoutesViewModel.getMyPlaceRoutes();
    }

    @Override
    public void onRouteClick(int id) {
        Intent intent = new Intent(getActivity(), SingleRoute.class);
        intent.putExtra("route_id",id);
        intent.putExtra("fromMy",true);
        getActivity().startActivity(intent);

    }
}
