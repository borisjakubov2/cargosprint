package com.example.kamioni.ui.options.settings;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.ViewModel;

import com.example.kamioni.repository.Repository;

public class SettingsViewModel extends ViewModel {

    private Repository repository;


    @ViewModelInject
    public SettingsViewModel(Repository repository) {
        this.repository = repository;
    }


    public String getLanguage(){
        return repository.getLanguage();
    }

    public void setLanguage(String language){
        repository.setLanguage(language);
    }


    public void setNotificationChecked(boolean notificationChecked){
        repository.setNotificationChecked(notificationChecked);
    }

    public boolean getNotificationChecked(){
        return repository.getNotificationChecked();
    }
}
