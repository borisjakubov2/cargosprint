package com.example.kamioni.repository;

import android.media.session.MediaSession;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kamioni.db.SharedPrefs;
import com.example.kamioni.db.UserDao;
import com.example.kamioni.model.LocalOffer;
import com.example.kamioni.model.LogoutResponse;
import com.example.kamioni.model.Offer;
import com.example.kamioni.model.ProfileResponse;
import com.example.kamioni.model.RegistrationRequest;
import com.example.kamioni.model.RouteResponse;
import com.example.kamioni.model.SingleRouteResponse;
import com.example.kamioni.model.TokenResponse;
import com.example.kamioni.model.UpdateOfferResponse;
import com.example.kamioni.model.UpdateUserResponse;
import com.example.kamioni.network.ApiService;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

@Singleton
public class Repository {

    ApiService apiService;
    UserDao kamioniDao;
    SharedPrefs sharedPrefs;



    @Inject
    public Repository(ApiService apiService, UserDao kamioniDao,SharedPrefs sharedPrefs) {
        this.apiService = apiService;
        this.kamioniDao = kamioniDao;
        this.sharedPrefs = sharedPrefs;

    }

    public void setLanguage(String language){
        sharedPrefs.setLanguage(language);
    }

    public String getLanguage(){
        return sharedPrefs.getLanguage();
    }

    public boolean  getNotificationChecked(){
        return sharedPrefs.isNotificationChecked();
    }

    public void setNotificationChecked(boolean notificationChecked){
        sharedPrefs.setNotificationChecked(notificationChecked);
    }

    public void insertUser(TokenResponse registrationResponse){
       sharedPrefs.setUser(registrationResponse);
    }

    public Observable<TokenResponse> getRefreshToken(String refreshToken){
        return apiService.getRefreshToken(refreshToken);
    }

    public Observable<TokenResponse> loginUser( String email, String pw ){

        return apiService.loginUser(email,pw);
    }

    public Observable<TokenResponse> register(RegistrationRequest registrationRequest){
        return apiService.registerUser(registrationRequest);
    }


    public Observable<LogoutResponse> logoutUser(){
        return  apiService.logoutUser(sharedPrefs.getAuth());
    }


    public String getTokenType(){
        return sharedPrefs.getTokenType();
    }

    public String getAccessToken(){
        return sharedPrefs.getAccessToken();
    }

    public String getRefreshToken(){
        return sharedPrefs.getRefreshToken();
    }

    public int getExpires(){
        return sharedPrefs.expires_in();
    }

    public void deleteUser(){
       sharedPrefs.removeUser();
    }

    public void insertOffer(LocalOffer offer){
        kamioniDao.insertOffer(offer);
    }

    public LiveData<LocalOffer> getOffer(){
        return kamioniDao.getOfferLocal();
    }




    public Observable<RouteResponse> getMyPlaceRoutes(int isFinish){
        return apiService.getMyTownRoutes(sharedPrefs.getAuth(),isFinish);
    }

    public Observable<RouteResponse> getMyLocationRoutes(Double lat, Double lng, int isFinish){
        return apiService.getMyLocationRoutes(sharedPrefs.getAuth(),lat,lng,isFinish);
    }

    public Observable<ProfileResponse> getMyProfile(){
        return apiService.getMyProfile(sharedPrefs.getAuth());
    }

    public Observable<UpdateUserResponse> updateUser(int radius){
        return apiService.updateProfile(sharedPrefs.getAuth(),radius);
    }

    public Observable<RouteResponse> getMyRoutes(){
        return apiService.getMyRoutes(sharedPrefs.getAuth());
    }

    public Observable<SingleRouteResponse> getSingleRoute(int offerId){
        return apiService.getSingleRoute(sharedPrefs.getAuth(),offerId);
    }

    public Observable<RouteResponse> getSearchRoutes(int maxweigth,int maxprice,String location){
        return apiService.getSearchRoutes(sharedPrefs.getAuth(),maxweigth,maxprice,location);
    }


    public void deleteRoute() {
        kamioniDao.deleteAll();
    }

    public Observable<UpdateOfferResponse> updateOfferStatus(int offer_id, String status){
        return apiService.updateOfferStatus(sharedPrefs.getAuth(),offer_id,status);
    }
}
