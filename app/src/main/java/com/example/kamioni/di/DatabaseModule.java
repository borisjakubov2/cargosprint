package com.example.kamioni.di;

import android.app.Application;
import androidx.room.Room;

import com.example.kamioni.db.AppDatabase;
import com.example.kamioni.db.SharedPrefs;
import com.example.kamioni.db.UserDao;
import com.example.kamioni.utils.Constants;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;


@Module
@InstallIn(ApplicationComponent.class)
public class DatabaseModule {

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Application application){
        return Room.databaseBuilder(application, AppDatabase.class, Constants.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @Provides
    @Singleton
    UserDao provideTokenDao(AppDatabase kamioniDatabase){
        return kamioniDatabase.kamioniDao();
    }


    @Singleton
    @Provides
    public static SharedPrefs provideSharedPrefs(Application application){
        return SharedPrefs.getInstance(application);
    }


}
