package com.example.kamioni.di;

import android.app.Application;

import com.example.kamioni.db.SharedPrefs;
import com.example.kamioni.network.ApiService;
import com.example.kamioni.utils.Constants;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

@Module
@InstallIn(ApplicationComponent.class)
public class NetworkModule {

    @Provides
    @Singleton
    public static ApiService provideMovieApiService(OkHttpClient okHttpClient){

        return  new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService.class);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkClient(Interceptor interceptor,HttpLoggingInterceptor loggingInterceptor){

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .build();

    }


    @Provides
    @Singleton
    public HttpLoggingInterceptor loggingInterceptor(){
        return new HttpLoggingInterceptor().setLevel(BODY);
    }


    @Provides
    @Singleton
    public Interceptor provideInterceptor(){

        return chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();

            requestBuilder.header("Content-Type", "application/json");
            requestBuilder.header("Accept", "application/json");
            return chain.proceed(requestBuilder.build());
        };

    }

}
