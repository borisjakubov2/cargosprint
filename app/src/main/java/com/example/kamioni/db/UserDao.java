package com.example.kamioni.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.example.kamioni.model.LocalOffer;


@Dao
public interface UserDao {

    @Insert
    void insertOffer(LocalOffer offerid);

    @Query("SELECT * FROM offers")
    LiveData<LocalOffer> getOfferLocal();


    @Query("DELETE FROM offers")
    void deleteAll();
}
