package com.example.kamioni.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.kamioni.model.LocalOffer;
import com.example.kamioni.model.Offer;
import com.example.kamioni.model.TokenResponse;

@Database(entities = {LocalOffer.class}, version = 4,exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao kamioniDao();
}
