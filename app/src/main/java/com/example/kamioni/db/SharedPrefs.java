package com.example.kamioni.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.UserHandle;

import com.example.kamioni.model.TokenResponse;

import java.util.HashMap;

import okhttp3.Request;
import retrofit2.http.Header;

public class SharedPrefs {

    private SharedPreferences prefs;
    private static SharedPrefs instance;

    private SharedPrefs(Context context) {
        prefs = context.getSharedPreferences("kamioni", Context.MODE_PRIVATE);

    }

    public static SharedPrefs getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefs(context);
        }
        return instance;
    }

    public void setUser(TokenResponse tokenResponse){
        setTokenType(tokenResponse.token_type);
        setAccessToken(tokenResponse.access_token);
        setExpiresIn(tokenResponse.expires_in);
        setRefreshToken(tokenResponse.refresh_token);
    }

    public void removeUser(){
        setTokenType("");
        setAccessToken("");
        setExpiresIn(0);
        setRefreshToken("");
    }

    public void setLanguage(String language) {prefs.edit().putString("lang_code",language).apply();}

    public String getLanguage(){
        return prefs.getString("lang_code","");
    }

    public void setTokenType(String tokenType){ prefs.edit().putString("user_token_type",tokenType).apply(); }

    public void setAccessToken(String token){ prefs.edit().putString("access_token",token).apply(); }

    public void setRefreshToken(String token){ prefs.edit().putString("refresh_token",token).apply(); }

    public void setExpiresIn(int expiresIn){ prefs.edit().putInt("token_expires",expiresIn).apply(); }

    public String getTokenType(){
        return prefs.getString("user_token_type","");
    }

    public String getRefreshToken(){
        return prefs.getString("refresh_token","");
    }

    public String getAccessToken(){ return prefs.getString("access_token",""); }

    public int expires_in(){ return prefs.getInt("token_expires",0); }


    public int getCurrentRoute() {
        return prefs.getInt("route_id",0);
    }

    public void saveRoute(int routeId) {
        prefs.edit().putInt("route_id",routeId).apply();
    }

    public  String getAuth(){
        return  "Bearer " + getAccessToken();
    }

    public void setNotificationChecked(boolean notificationChecked){
        prefs.edit().putBoolean("not_checked", notificationChecked).apply();;
    }

    public boolean isNotificationChecked(){
        return prefs.getBoolean("not_checked",true);
    }

}
