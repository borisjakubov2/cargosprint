package com.example.kamioni.utils;

public class Constants {
    public static final String BASE_URL = "https://holding.devrockit.com";
    public static final String REGISTER_URL = BASE_URL + "/api/register";
    public static final String REFRESH_TOKEN_URL = BASE_URL + "/api/refresh-token";
    public static final String LOGIN_URL = BASE_URL + "/api/login";
    public static final String DATABASE_NAME = "kamioni.database";
    public static final String LOGOUT_URL = BASE_URL+"/api/logout";
    public static final String MY_ROUTES = BASE_URL + "/api/get-my-offers";
    public static final String MY_LOCATION_ROUTES = BASE_URL + "/api/get-offers-by-current-location";
    public static final String MY_PROFILE = BASE_URL + "/api/my-profile";
    public static final String MY_PROFILE_UPDATE = BASE_URL + "/api/update-profile";
    public static final String MY_PLACE_ROUTES = BASE_URL + "/api/get-offers-by-home-location";
    public static final String SINGLE_ROUTE = BASE_URL + "/api/get-offer-by-id";
    public static final String CUSTOM_SEARCH_ROUTE = BASE_URL+ "/api/get-offers-with-filters";
    public static final String UPDATE_OFFER_STATUS = BASE_URL + "/api/update-offer-status";
}
