package com.example.kamioni.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

import retrofit2.HttpException;

public class ErrorHandler {

    String message;



    public static String getErrorMessage(Throwable e){
        String message = "";
        Object value = null;
        try {
            if (e instanceof IOException) {
                message = "No internet connection!";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                String errorBody = error.response().errorBody().string();
                JSONObject jObj = new JSONObject(errorBody);

                message = jObj.getString("message");
                JSONObject  msg = jObj.getJSONObject("errors");

                Iterator<String> temp = msg.keys();
                while (temp.hasNext()) {
                    String key = temp.next();
                    value = msg.get(key);

                }


            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if(value==null){
            return message;
        }else{
            return value.toString();
        }

    }
}
