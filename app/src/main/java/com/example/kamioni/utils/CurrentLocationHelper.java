package com.example.kamioni.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.List;

public class CurrentLocationHelper extends LocationCallback {

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationSetListener locationSetListener;
    private final Context context;

    public interface  LocationSetListener{
        void onLocationUpdate(Location location);
    }

    public void setLocationSetListener(LocationSetListener locationSetListener) {
        this.locationSetListener = locationSetListener;
    }

    public CurrentLocationHelper(Context context) {
        this.context = context;
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        List<Location> locations = locationResult.getLocations();
        Location mLastLocation = locations.get(locations.size() - 1);

        if(mLastLocation!=null){
            mLastLocation.setAccuracy(1);
            locationSetListener.onLocationUpdate(locationResult.getLastLocation());
            fusedLocationProviderClient.removeLocationUpdates(this);
        }

    }

    @SuppressLint("MissingPermission")
    public void requestNewLocationData() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        /* 0.5 secs */
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setSmallestDisplacement(10);


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);

        fusedLocationProviderClient.requestLocationUpdates(
                mLocationRequest, this, Looper.myLooper());

    }


}