package com.example.kamioni.network;

import com.example.kamioni.model.LogoutResponse;
import com.example.kamioni.model.UpdateOfferResponse;
import com.example.kamioni.model.ProfileResponse;
import com.example.kamioni.model.RegistrationRequest;
import com.example.kamioni.model.RouteResponse;
import com.example.kamioni.model.SingleRouteResponse;
import com.example.kamioni.model.TokenResponse;
import com.example.kamioni.model.UpdateUserResponse;
import com.example.kamioni.utils.Constants;


import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {


    @POST(Constants.REGISTER_URL)
    Observable<TokenResponse> registerUser(@Body RegistrationRequest registrationRequest);

    @FormUrlEncoded
    @POST(Constants.REFRESH_TOKEN_URL)
    Observable<TokenResponse> getRefreshToken(@Field("refreshToken") String refreshToken);


    @FormUrlEncoded
    @POST(Constants.LOGIN_URL)
    Observable<TokenResponse> loginUser(@Field("email") String email, @Field("password") String pw);

    @POST(Constants.LOGOUT_URL)
    Observable<LogoutResponse> logoutUser(@Header("Authorization") String authorization);


    @GET(Constants.MY_PLACE_ROUTES)
    Observable<RouteResponse> getMyTownRoutes(@Header("Authorization") String authorization, @Query("is_finish") int isFinish);

    @GET(Constants.MY_LOCATION_ROUTES)
    Observable<RouteResponse> getMyLocationRoutes(@Header("Authorization") String authorization, @Query("lat") Double lat, @Query("lng") Double lng, @Query("is_finish") int isFinish);


    @GET(Constants.MY_PROFILE)
    Observable<ProfileResponse> getMyProfile(@Header("Authorization") String authorization);

    @POST(Constants.MY_PROFILE_UPDATE)
    Observable<UpdateUserResponse> updateProfile(@Header("Authorization") String authorization, @Query("radius") int radius);

    @GET(Constants.MY_ROUTES)
    Observable<RouteResponse> getMyRoutes(@Header("Authorization") String authorization);

    @GET(Constants.SINGLE_ROUTE)
    Observable<SingleRouteResponse> getSingleRoute(@Header("Authorization") String authorization, @Query("offer_id") int offer_id);


    @POST(Constants.CUSTOM_SEARCH_ROUTE)
    Observable<RouteResponse> getSearchRoutes(@Header("Authorization") String authorization, @Query("max_price") int maxprice, @Query("max_weight") int maxweight, @Query("location")String location);


    @POST(Constants.UPDATE_OFFER_STATUS)
    Observable<UpdateOfferResponse> updateOfferStatus(@Header("Authorization") String authorization, @Query("offer_id") int offer_id, @Query("status") String status);
}
