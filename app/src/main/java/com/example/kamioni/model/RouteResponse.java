package com.example.kamioni.model;

import java.util.ArrayList;

public class RouteResponse {

    public ArrayList<Offer> data;
    public Links links;
    public Meta meta;
}
