package com.example.kamioni.model;

public class Offer {
    public int id;
    public String tour_name;
    public String type_for_transport;
    public String start_location;
    public String finish_location;
    public String start_time;
    public String finish_time;
    public int price;
    public int weight;
    public String status;
    public CompanyId company_id;
    

    public CompanyId getCompanyId() {
        return company_id;
    }

    public void setCompanyId(CompanyId companyId) {
        this.company_id = companyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTour_name() {
        return tour_name;
    }

    public void setTour_name(String tour_name) {
        this.tour_name = tour_name;
    }

    public String getType_for_transport() {
        return type_for_transport;
    }

    public void setType_for_transport(String type_for_transport) {
        this.type_for_transport = type_for_transport;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }

    public String getFinish_location() {
        return finish_location;
    }

    public void setFinish_location(String finish_location) {
        this.finish_location = finish_location;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

}
