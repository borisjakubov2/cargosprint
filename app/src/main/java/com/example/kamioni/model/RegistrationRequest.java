package com.example.kamioni.model;

public class RegistrationRequest {
    
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
    public String password;
    public String password_confirmation;
    public String address;
    public int radius;
    public String job;

}
