package com.example.kamioni.model;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "offers")
public class LocalOffer {
    @PrimaryKey
    public int id;
    public int localId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }
}
