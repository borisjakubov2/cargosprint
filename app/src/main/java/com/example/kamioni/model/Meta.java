package com.example.kamioni.model;

import java.util.List;

public class Meta{
    public int current_page;
    public int from;
    public int last_page;
    public List<Links> links;
    public String path;
    public int per_page;
    public int to;
    public int total;
}