package com.example.kamioni.model;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "token_response")
public class TokenResponse {

    @PrimaryKey
    @NonNull
    public String token_type;
    public String access_token;
    public String refresh_token;
    public int expires_in;

    public TokenResponse(@NonNull String token_type, String access_token, String refresh_token, int expires_in) {
        this.token_type = token_type;
        this.access_token = access_token;
        this.refresh_token = refresh_token;
        this.expires_in = expires_in;
    }

}
