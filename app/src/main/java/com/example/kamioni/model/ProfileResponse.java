package com.example.kamioni.model;

public class ProfileResponse {
    public int id;
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
    public String job;
    public String address;
    public int radius;
}
