package com.example.kamioni.model;

public class Links{
    public String first;
    public String last;
    public Object prev;
    public Object next;
    public String url;
    public Object label;
    public boolean active;
}